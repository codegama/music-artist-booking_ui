/* =================================
  BANNER ROTATOR IMAGE 
  =================================== */
var slides = $(".full-screen"),
    b = slides.find('.item');
b.each(function() {
    var e = $(this),
        ocImg = e.find('img').attr('src');
    e.css({ 'background-image': 'url(' + ocImg + ')' });
});

slides.owlCarousel({
    // stagePadding: 50,
    loop: true,
    // margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    dots: false,
    nav: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    navigationText: [
        "<i class='fas fa-chevron-left'></i>",
        "<i class='fas fa-chevron-right'></i>"
    ],
    items: 1,
});

/* =================================
 SCROLL TO
 =================================== */
$('a[href^="#"]').on('click', function(event) {

    var target = $(this.getAttribute('href'));

    if (target.length) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});

// scroll top nav

$(document).ready(function() {
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $(".navbar-fixed-top").addClass("nav-color");
        } else {
            $(".navbar-fixed-top").removeClass("nav-color");
        }
    });
});

